# Video

### Installation

Clone repo

Install the dependencies and devDependencies and start the server.

```sh
$ cd video-backoffice
$ composer install
$ php artisan migrate
$ php artisan passport:install
$ php artisan db:seed
```

Install Supervisor for listening to user transaction execution(For charging user monthly)

Here the documentation: 
https://laravel.com/docs/5.6/queues#supervisor-configuration
