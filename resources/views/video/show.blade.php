@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">View Video</h1>
                    </div>
                </div> 
            </div>
        </div>

        <section class="content">
            <div class="row px-2">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">{{ $video->title}}</div>
                        <div class="card-body">

                            <a href="{{ url('/video') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                            <a href="{{ url('/video/' . $video->id . '/edit') }}" title="Edit video"><button class="btn btn-primary btn-sm"><i class="fa fa-edit" aria-hidden="true"></i> Edit</button></a>

                            <form method="POST" action="{{ url('video' . '/' . $video->id) }}" accept-charset="UTF-8" style="display:inline">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-danger btn-sm" title="Delete video" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                            </form>
                            <br/>
                            <br/>

                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th>ID</th><td>{{ $video->id }}</td>
                                        </tr>
                                        <tr><th> Title </th><td> {{ $video->title }} </td></tr><tr><th> Description </th><td> {{ $video->description }} </td></tr><tr><th> Link </th><td> {{ $video->link }} </td></tr><tr><th> Category </th><td> <a href="{{ url('/category/' . $video->category->id) }}">{{ $video->category->name }}</a> </td></tr>
                                        <tr>
                                            <th>Type</th>
                                            <td>
                                                @if ($video->type === 'PAID')
                                                <span class="badge bg-success">{{ $video->type }}</span>
                                                @elseif ($video->type === 'FREE')
                                                <span class="badge bg-black">{{ $video->type }}</span>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Thumbnail</th>
                                            <td>
                                                <img src="{{ asset('/uploads/' . $video->photo) }}" width="100%">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
