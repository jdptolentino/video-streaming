@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Create New Video</h1>
                    </div>
                </div> 
            </div>
        </div>
        <section class="content">
            <div class="row px-2">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">Create New Video</div>
                        <div class="card-body">
                            <a href="{{ url('/video') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                            <br />
                            <br />

                            @if ($errors->any())
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <li class="ml-2">{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
                            @if(count($categories) == 0)
                                <div class="alert alert-danger">No categories found. Please <a href="{{ url('/category/create') }}">add category</a> to proceed.</div>
                            @else
                                <form method="POST" action="{{ url('/video') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                    {{ csrf_field() }}

                                    @include ('video.form', ['formMode' => 'create'])

                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
