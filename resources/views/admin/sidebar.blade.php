<!-- Sidebar Menu -->
<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

      <li class="nav-item">
        <a href="{{ url('/') }}" class="nav-link @if(request()->is('/')) active @endif" >
          <i class="nav-icon fas fa-tachometer-alt"></i>
          <p>
            Dashboard
          </p>
        </a>
      </li>
      @if(Auth::user()->type == 'A')
        <li class="nav-item">
          <a href="{{ url('/admin') }}" class="nav-link @if(request()->is('admin') || request()->is('admin/*')) active @endif" >
            <i class="nav-icon fas fa-user-tie"></i>
            <p>
              Admin
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ url('/user') }}" class="nav-link @if(request()->is('user') || request()->is('user/*')) active @endif">
            <i class="nav-icon fas fa-users"></i>
            <p>
              Users
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ url('/video') }}" class="nav-link @if(request()->is('video') || request()->is('video/*')) active @endif">
            <i class="nav-icon fas fa-video"></i>
            <p>
              Videos
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ url('/category') }}" class="nav-link @if(request()->is('category') || request()->is('category/*')) active @endif">
            <i class="nav-icon fas fa-list-alt"></i>
            <p>
              Categories
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ url('/series') }}" class="nav-link @if(request()->is('series') || request()->is('series/*')) active @endif">
            <i class="nav-icon fas fa-list"></i>
            <p>
              Series
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ url('/plan') }}" class="nav-link @if(request()->is('plan') || request()->is('plan/*')) active @endif">
            <i class="nav-icon fas fa-money-check"></i>
            <p>
              Subscription Plans
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ url('/transactions') }}" class="nav-link @if(request()->is('transactions') || request()->is('transactions/*')) active @endif">
            <i class="nav-icon fas fa-file-invoice"></i>
            <p>
              Transactions
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ url('/requests') }}" class="nav-link @if(request()->is('requests') || request()->is('requests/*')) active @endif">
            <i class="nav-icon fas fa-question-circle"></i>
            <p>
              Requests
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ url('/reports') }}" class="nav-link @if(request()->is('reports') || request()->is('reports/*')) active @endif">
            <i class="nav-icon fas fa-folder-open"></i>
            <p>
              Reports
            </p>
          </a>
        </li>
      @endif
      <li class="nav-item">
        <form method="POST" action="{{route('logout')}}" id="logout-form" style="display:none">
          @csrf
        </form>
        <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit()">
          <i class="nav-icon fas fa-sign-out-alt"></i>
          <p>
            Logout
          </p>
        </a>
      </li>
    </ul>
  </nav>
