<div class="form-group {{ $errors->has('first_name') ? 'has-error' : ''}}">
    <label for="first_name" class="control-label">{{ 'First Name' }}</label>
    <input class="form-control" name="first_name" type="text" id="first_name" value="{{ isset($admin->first_name) ? $admin->first_name : old('first_name') }}" >
    {!! $errors->first('first_name', '<p class="text-danger help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('middle_name') ? 'has-error' : ''}}">
    <label for="middle_name" class="control-label">{{ 'Middle Name' }}</label>
    <input class="form-control" name="middle_name" type="text" id="middle_name" value="{{ isset($admin->middle_name) ? $admin->middle_name : old('middle_name')}}" >
    {!! $errors->first('middle_name', '<p class="text-danger help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('last_name') ? 'has-error' : ''}}">
    <label for="last_name" class="control-label">{{ 'Last Name' }}</label>
    <input class="form-control" name="last_name" type="text" id="last_name" value="{{ isset($admin->last_name) ? $admin->last_name : old('last_name')}}" >
    {!! $errors->first('last_name', '<p class="text-danger help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="control-label">{{ 'Email' }}</label>
    <input class="form-control" name="email" type="text" id="email" value="{{ isset($admin->email) ? $admin->email : old('email')}}" autocomplete="off">
    {!! $errors->first('email', '<p class="text-danger help-block">:message</p>') !!}
</div>

@if (!isset($admin->password))
    <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
        <label for="password" class="control-label">{{ 'Password' }}</label>
        <input class="form-control" name="password" type="password" id="password" value="{{ isset($admin->password) ? $admin->password : ''}}" autocomplete="off">
        {!! $errors->first('password', '<p class="text-danger help-block">:message</p>') !!}
    </div>
    <div class="form-group">
        <label for="c_password" class="control-label">{{ 'Confirm Password' }}</label>
        <input class="form-control" name="password_confirmation" type="password" id="c_password"  autocomplete="off">
    </div>
@endif

<div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
    <label for="type" class="control-label">{{ 'Type' }}</label>
    <select class="form-control" name="type" id="type">
            <option {{ isset($admin->type) && $admin->type == 'A' ? 'selected': ''}} value="A">Admin</option>
            <option {{ isset($admin->type) && $admin->type == 'P' ? 'selected': ''}} value="P">Partner</option>
        </select>
    {!! $errors->first('type', '<p class="text-danger help-block">:message</p>') !!}
</div>

<!-- <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
    <label for="password" class="control-label">{{ 'Password' }}</label>
    <input class="form-control" name="password" type="text" id="password" value="{{ isset($admin->password) ? $admin->password : ''}}" >
    {!! $errors->first('password', '<p class="text-danger help-block">:message</p>') !!}
</div> -->


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
