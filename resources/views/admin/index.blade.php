@extends('layouts.app')

@section('content')

    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Admin</h1>
                    </div>
                </div>
            </div>
        </div>
        
        <section class="content">
            <div class="row px-2">
                @if(Session::has('flash_message'))
                <div class="col-lg-12">
                    <div class="alert alert-success px-2">
                        <span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
                    </div>
                @endif
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{ url('/admin/create') }}" title="Back"><button class="btn btn-primary btn-sm mt-n2 mb-n1"><i class="fa fa-plus" aria-hidden="true"></i> Add New</button></a>
                            <div class="card-tools">
                                <form method="GET" action="{{ url('/admin') }}">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                                    </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                    <tr>
                                        <th>First Name</th><th>Middle Name</th><th>Last Name</th><th>Status</th><th>Type</th><th width="100">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(count($admin) == 0)
                                    <tr><td colspan="100%"><div class="alert alert-danger">No data found</div></td></tr>
                                @else
                                    @foreach($admin as $item)
                                        <tr>
                                            <!-- <td>{{ $item->id }}</td> -->
                                            <td>{{ $item->first_name }}</td><td>{{ $item->middle_name }}</td><td>{{ $item->last_name }}</td>

                                            <td>
                                                @if ($item->status === 'ACTIVE')
                                                <span class="badge bg-primary">{{ $item->status }}</span>
                                                @elseif ($item->status === 'DELETED')
                                                <span class="badge bg-danger">{{ $item->status }}</span>
                                                @elseif ($item->status === 'SUSPEND')
                                                <span class="badge bg-warning">{{ $item->status }}</span>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($item->type === 'A')
                                                <span class="badge bg-primary">Admin</span>
                                                @elseif ($item->type === 'P')
                                                <span class="badge bg-info">Partner</span>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ url('/admin/' . $item->id) }}" title="View admin"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                                <a href="{{ url('/admin/' . $item->id . '/edit') }}" title="Edit admin"><button class="btn btn-primary btn-sm"><i class="fa fa-edit" aria-hidden="true"></i> Edit</button></a>

                                                <form method="POST" action="{{ url('/admin' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                    {{ method_field('DELETE') }}
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete admin" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            @if(count($admin) > 0)
                                <div class="pagination-wrapper px-3"> {!! $admin->appends(['search' => Request::get('search')])->render() !!} </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
