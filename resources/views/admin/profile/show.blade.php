@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Profile</h1>
                    </div>
                </div> 
            </div>
        </div>

        <section class="content">
            <div class="row px-2">
                @if(Session::has('flash_message'))
                    <div class="col-lg-12">
                        <div class="alert alert-success px-2">
                            <span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em>
                        </div>
                    </div>
                @endif
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">{{ $admin->first_name }} {{ $admin->middle_name }} {{ $admin->last_name }}</div>
                        <div class="card-body">

                            <a href="{{ url('/profile/edit') }}" title="Edit admin"><button class="btn btn-primary btn-sm"><i class="fa fa-edit aria-hidden="true"></i> Edit</button></a>

                            <form method="POST" action="{{ url('admin' . '/' . $admin->id) }}" accept-charset="UTF-8" style="display:inline">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                            </form>
                            <br/>
                            <br/>

                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th width="50%">ID</th><td>{{ $admin->id }}</td>
                                        </tr>
                                        <tr><th> First Name </th><td> {{ $admin->first_name }} </td></tr><tr><th> Middle Name </th><td> {{ $admin->middle_name }} </td></tr><tr><th> Last Name </th><td> {{ $admin->last_name }} </td></tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
