@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1 class="m-0 text-dark">View User</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <section class="content">
        <div class="row px-2">

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">{{ $user->user_name }}</div>
                    <div class="card-body">

                        <a href="{{ url('/user') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/user/' . $user->id . '/edit') }}" title="Edit user"><button class="btn btn-primary btn-sm"><i class="fa fa-edit" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('user' . '/' . $user->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete user" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                        </form>
                        @if(!$user->activePlans)
                          <button type="button"class="btn btn-info btn-sm text-white float-right" title="Grant access" data-toggle="modal" data-target="#grant-access">
                              Grant access
                          </button>
                        @endif
                        <br/>
                        <br/>


                          <div class="modal fade" id="grant-access" tabindex="-1" role="dialog" aria-labelledby="grant-access" aria-hidden="true">
                            <div class="modal-dialog">
                                <form method="POST" action="{{ url('/requests/back/' . $user->id) }}" accept-charset="UTF-8">
                                    {{ csrf_field() }}
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="grant-access">Grant access</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                        <label for="">Select Plan</label>
                                        <select name="plan" id="" class="form-control" required>
                                            @foreach($subscriptions as $v)
                                                <option value="{{ $v->id }}">{{ $v->title }} - {{ number_format($v->amount,2) }}</option>
                                            @endforeach
                                        </select>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary submit">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                    <th width="50%">ID</th><td>{{ $user->id }}</td>
                                    </tr>
                                    <tr><th> Username </th><td> {{ $user->user_name }} </td></tr><tr><th> Email </th><td> {{ $user->email }} </td></tr><tr><th> First Name </th><td> {{ $user->first_name }} </td></tr><tr><th> Middle Name </th><td> {{ $user->middle_name }} </td></tr><tr><th> Last Name </th><td> {{ $user->last_name }} </td></tr><tr>
                                        <th> Status </th>
                                        <td>
                                            @if ($user->status === 'ACTIVE')
                                            <span class="badge bg-primary">{{ $user->status }}</span>
                                            @elseif ($user->status === 'DELETED')
                                            <span class="badge bg-danger">{{ $user->status }}</span>
                                            @elseif ($user->status === 'SUSPEND')
                                            <span class="badge bg-warning">{{ $user->status }}</span>
                                            @elseif ($user->status === 'EMAIL_VERIFICATION')
                                            <span class="badge bg-success">{{ $user->status }}</span>
                                            @endif    
                                        </td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row px-2">
            <div class="col-12">
              <div class="card">
                <div class="card-body table-responsive p-0">
                  <table class="table table-hover text-nowrap">
                    <thead>
                      <tr>
                        <th>Subscription</th><th>Amount</th><th>Status</th><th>Availed Date</th>
                      </tr>
                    </thead>
                    <tbody>
                      @if(count($user->transactions) == 0)
                              <tr><td colspan="100%"><div class="alert alert-danger">No data found</div></td></tr>
                        @else
                            @foreach($user->transactions as $txn)
                              <tr>
                                  <!-- <td>{{ $txn->id }}</td> -->
                                  <td>{{ $txn->plan->title }}</td>
                                  <td>{{ number_format($txn->amount,2) }}</td>
                                <td>
                                  @if ($txn->status === 'PENDING')
                                  <span class="badge bg-primary">{{ $txn->status }}</span>
                                  @elseif ($txn->status === 'FAILED')
                                  <span class="badge bg-danger">{{ $txn->status }}</span>
                                  @elseif ($txn->status === 'DECLINED')
                                  <span class="badge bg-warning">{{ $txn->status }}</span>
                                  @elseif ($txn->status === 'PAID')
                                  <span class="badge bg-success">{{ $txn->status }}</span>
                                  @endif
                                </td>
                                <td>{{ date('M d, Y', strtotime($txn->created_at)) }}</td>
                              </tr>
                            @endforeach
                          @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
    </section>
</div>

@endsection
