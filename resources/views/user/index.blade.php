@extends('layouts.app')

@section('content')
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Users</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>


      <section class="content">
        <div class="row px-2">
          @if(Session::has('flash_message'))
            <div class="col-lg-12">
              <div class="alert alert-success px-2">
                  <span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em>
              </div>
            </div>
          @endif
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  {{-- <h3 class="card-title">User List</h3> --}}
                  <a href="{{ url('/user/create') }}" title="Back"><button class="btn btn-primary btn-sm mt-n2 mb-n1"><i class="fa fa-plus" aria-hidden="true"></i> Add New</button></a>
                  <div class="card-tools">
                    <form method="GET" action="{{ url('/user') }}">
                        <div class="input-group input-group-sm" style="width: 150px;">
                          <input type="text" name="table_search" class="form-control float-right" placeholder="Search">
                          <div class="input-group-append">
                            <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                          </div>
                        </div>
                    </form>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                  <table class="table table-hover text-nowrap">
                    <thead>
                      <tr>
                        <th>Name</th><th>Email</th><th>Status</th><th width="100">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                        @if(count($user) == 0)
                              <tr><td colspan="100%"><div class="alert alert-danger">No data found</div></td></tr>
                        @else
                            @foreach($user as $item)
                                <tr>
                                  <td>{{ $item->full_name }}</td>
                                  <td>{{ $item->email }}</td>
                                  

                                  <td>
                                    @if ($item->status === 'ACTIVE')
                                    <span class="badge bg-primary">{{ $item->status }}</span>
                                    @elseif ($item->status === 'DELETED')
                                    <span class="badge bg-danger">{{ $item->status }}</span>
                                    @elseif ($item->status === 'SUSPEND')
                                    <span class="badge bg-warning">{{ $item->status }}</span>
                                    @elseif ($item->status === 'EMAIL_VERIFICATION')
                                    <span class="badge bg-success">{{ $item->status }}</span>
                                    @endif
                                  
                                  </td>
                                    <td>
                                        <a href="{{ url('/user/' . $item->id) }}" title="View user"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                        <a href="{{ url('/user/' . $item->id . '/edit') }}" title="Edit user"><button class="btn btn-primary btn-sm"><i class="fa fa-edit" aria-hidden="true"></i> Edit</button></a>

                                        <form method="POST" action="{{ url('/user' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-danger btn-sm" title="Delete user" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                  </table>
                  @if(count($user) > 0)
                    <div class="pagination-wrapper px-3"> {!! $user->appends(['search' => Request::get('search')])->render() !!} </div>
                  @endif
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
          </div>
      </section>
    </div>
    <!-- /.content-header -->

@endsection
