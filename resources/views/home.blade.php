@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-lg-12">
                        <h1 class="m-0 text-dark">Dashboard</h1>
                    </div>
                </div> 
            </div>
        </div>

        <section class="content">
        <div class="row px-2">
          <div class="col-lg-3">
            <!-- small card -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{ number_format($countTxn) }}</h3>

                <h5>Total Paid Subscription</h5>
              </div>
              <div class="icon">
                <i class="fas fa-money-check-alt"></i>
              </div>
              @if(Auth::user()->type == 'A')
                <a href="{{ url('/transactions') }}" class="small-box-footer">
                  More info <i class="fas fa-arrow-circle-right"></i>
                </a>
              @endif
            </div>
          </div>

          <!-- ./col -->
          <div class="col-lg-3">
            <!-- small card -->
            <div class="small-box bg-warning">
              <div class="inner text-white">
                <h3>{{ number_format($countUsers) }}</h3>

                <h5>User Registrations</h5>
              </div>
              <div class="icon">
                <i class="fas fa-user-plus"></i>
              </div>
              @if(Auth::user()->type == 'A')
                <a href="{{ url('/user') }}" class="small-box-footer">
                  <span class="text-white">More info</span> <i class="fas fa-arrow-circle-right text-white"></i>
                </a>
              @endif
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3">
            <!-- small card -->
            <div class="small-box bg-danger">
              <div class="inner">
              <h3>{{ number_format($countVideos) }}</h3>

                <h5>Videos</h5>
              </div>
              <div class="icon">
                <i class="fas fa-video"></i>
              </div>
              @if(Auth::user()->type == 'A')
              <a href="{{ url('/video') }}" class="small-box-footer">
                  More info <i class="fas fa-arrow-circle-right"></i>
                </a>
                @endif
            </div>
          </div>
          <!-- ./col -->

          <div class="col-lg-3">
            <!-- small card -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{ number_format($countSales,2) }}</h3>

                <h5>Total Sales</h5>
              </div>
              <div class="icon">
                <i class="fas fa-money-bill-alt"></i>
              </div>
              @if(Auth::user()->type == 'A')
            <a href="#" class="small-box-footer">
                <!-- More info <i class="fas fa-arrow-circle-right"></i> -->
                &nbsp;
              </a>
              @endif
            </div>
          </div>
          
          <div class="col-lg-3">
            <!-- small card -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{ number_format($lastMonthSales,2) }}</h3>

                <h5>Last Month Sales - {{ date("F Y",strtotime("-1 month")) }}</h5>
              </div>
              <div class="icon">
                <i class="fas fa-money-bill-alt"></i>
              </div>
              @if(Auth::user()->type == 'A')
            <a href="#" class="small-box-footer">
                <!-- More info <i class="fas fa-arrow-circle-right"></i> -->
                &nbsp;
              </a>
              @endif
            </div>
          </div>

          <div class="col-lg-3">
            <!-- small card -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{ number_format($currentMonthSales,2) }}</h3>

                <h5>Current Month Sales - {{ date("F Y") }}</h5>
              </div>
              <div class="icon">
                <i class="fas fa-money-bill-alt"></i>
              </div>
              @if(Auth::user()->type == 'A')
            <a href="#" class="small-box-footer">
                <!-- More info <i class="fas fa-arrow-circle-right"></i> -->
                &nbsp;
              </a>
              @endif
            </div>
          </div>
          @if(Auth::user()->type == 'A')
          <!-- latest transactions -->
            <div class="col-lg-9">
              <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Latest Transactions</h3>
                    <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                      <i class="fas fa-minus"></i>
                    </button>
                  </div>
                </div>
                <div class="card-body table-responsive p-0">
                  <table class="table table-hover text-nowrap table-border">
                      <thead>
                          <tr>
                              <th>Name</th><th>Subscription</th><th>Amount</th><th>Status</th><th>Date</th><th width="100">Actions</th>
                          </tr>
                      </thead>
                      <tbody>
                      @if(count($transactions) == 0)
                          <tr><td colspan="100%"><div class="alert alert-danger">No data found</div></td></tr>
                      @else
                          @foreach($transactions as $item)
                              <tr>
                                  <!-- <td>{{ $item->id }}</td> -->
                                  <td><a href="{{ url('/user/' . $item->user['id']) }}">{{ $item->user['first_name'] }} {{ $item->user['last_name'] }}</a></td>
                                  <td>{{ $item->plan->title }}</td><td>{{ number_format($item->amount,2) }}</td>
                                  <td>
                                      @if ($item->status === 'PAID')
                                      <span class="badge bg-success">{{ $item->status }}</span>
                                      @elseif ($item->status === 'FAILED')
                                      <span class="badge bg-black">{{ $item->status }}</span>
                                      @elseif ($item->status === 'DECLINED')
                                      <span class="badge bg-danger">{{ $item->status }}</span>
                                      @elseif ($item->status === 'PENDING')
                                      <span class="badge bg-warning">{{ $item->status }}</span>
                                      @endif
                                  </td>
                                  <td>{{ date('d F Y', strtotime($item->created_at)) }}</td>
                                  <td>
                                      <a href="{{ url('/transactions/invoice/' . $item->id) }}" title="View Invoice"><button class="btn btn-success btn-sm"><i class="fa fa-file" aria-hidden="true"></i> Invoice</button></a>
                                      <a href="{{ url('/transactions/' . $item->id) }}" title="View transaction"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                  </td>
                              </tr>
                          @endforeach
                      @endif
                      </tbody>
                  </table>
                </div>
                <div class="card-footer clearfix">
                  <a href="{{ url('/transactions') }}" class="uppercase float-right">View All Transactions</a>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
            <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Recently Registered Users</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                      <i class="fas fa-minus"></i>
                    </button>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                  <ul class="products-list product-list-in-card pl-2 pr-2">
                    @foreach($users as $user)
                      <li class="item">
                        <div class="product-img">
                          <img class="img-responsive img-circle img-size-50" src="{{ asset('images/admin.png') }}">
                        </div>
                        <div class="product-info">
                          <a href="{{ url('/user/' . $user->id) }}" class="product-title">{{ $user->first_name }} {{ $user->last_name }}</a>
                          <span class="product-description">
                            {{ $user->email }}
                          </span>
                        </div>
                      </li>
                    @endforeach
                  </ul>
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-center">
                  <a href="{{ url('/user') }}" class="uppercase">View All Users</a>
                </div>
                <!-- /.card-footer -->
              </div>
            </div>
          </div>
          <div class="col-lg-9">
            <div class="card">
              <div class="card-header">
                  <h3 class="card-title">Latest Request</h3>
                  <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap table-border">
                  <thead>
                    <tr>
                        <th>Name</th><th>Email</th><th>Subscription</th><th>Amount</th><th>Status</th><th width="100">Actions</th>
                    </tr>
                </thead>
                <tbody>
                @if(count($requests) == 0)
                    <tr><td colspan="100%"><div class="alert alert-danger">No data found</div></td></tr>
                @else
                    @foreach($requests as $item)
                        <tr>
                            <!-- <td>{{ $item->id }}</td> -->
                            <td>{{ $item->user['first_name'] }} {{ $item->user['last_name'] }}</td><td>{{ $item->user['email'] }}</td><td>{{ $item->plan->title }}</td><td>{{ number_format($item->plan->amount,2) }}</td>
                            <td>
                                <span class="badge bg-warning">{{ $item->status }}</span>
                            </td>
                            <td>
                              <button type="button"class="btn btn-warning btn-sm text-white" title="Grant access" data-toggle="modal" data-target="#modal{{$item->id}}">
                                <i class="fa fa-check" aria-hidden="true"></i> Grant access
                              </button>

                              <div class="modal fade" id="modal{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <form method="POST" action="{{ url('/requests' . '/' . $item->id. '/grant') }}" accept-charset="UTF-8">
                                        {{ csrf_field() }}
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Grant access</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                Are you sure to grant access?
                                                <br>
                                                <br>
                                            <div><b>Subscription: {{ $item->plan->title }}</b></div> 
                                            <div><b>User amount paid: {{ number_format($item->plan->amount,2) }}</b></div> 
                                            <br>
                                            <label for="">Confirm subscription</label><br>
                                            <select name="plan" id="" class="form-control">
                                                @foreach($subscriptions as $v)
                                                    <option {{ $item->plan->id === $v->id ? 'selected' : '' }} value="{{ $v->id }}">{{ $v->title }} - {{ number_format($v->amount,2) }}</option>
                                                @endforeach
                                            </select>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary submit">Accept</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                              </div>

                              <!-- delete start-->
                              <button type="button"class="btn btn-danger btn-sm" title="Delete request" data-toggle="modal" data-target="#deleteModal{{$item->id}}">
                                  <i class="fa fa-trash" aria-hidden="true"></i> Delete
                              </button>

                              <div class="modal fade" id="deleteModal{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                  <div class="modal-dialog">
                                      <form method="POST" action="{{ url('/requests' . '/destroy/' . $item->id) }}" accept-charset="UTF-8">
                                          {{ csrf_field() }}
                                          <div class="modal-content">
                                              <div class="modal-header">
                                                  <h5 class="modal-title" id="exampleModalLabel">Delete request</h5>
                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                  </button>
                                              </div>
                                              <div class="modal-body">
                                                  Are you sure to delete this request?
                                                  <br>
                                                  <br>
                                                  <div><b>Subscription: {{ $item->plan->title }}</b></div> 
                                                  <div><b>User amount paid: {{ number_format($item->plan->amount,2) }}</b></div>
                                              </div>
                                              <div class="modal-footer">
                                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                  <button type="button" class="btn btn-primary submitDelete">Delete</button>
                                              </div>
                                          </div>
                                      </form>
                                  </div>
                              </div>
                              <!-- delete end -->
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
                </table>
              </div>
              <div class="card-footer clearfix">
                <a href="{{ url('/requests') }}" class="uppercase float-right">View All Requests</a>
              </div>
            </div>
          </div>
        @endif
        <!-- /.row -->
        </section>
    </div>
@endsection

@push('custom-scripts')
<script>
$(document).ready(function () {   
    $('button.submit').on('click', function () {   
    var myForm =  $(this).closest("form");   
        if (myForm) {   
            $(this).text('Loading..')
            myForm.submit()
            myForm.find('button, select').prop('disabled', true);   
        }   
    });   

     // delete requests
     $('button.submitDelete').on('click', function () {   
        var myDeleteForm =  $(this).closest("form");   
            if (myDeleteForm) {   
                $(this).text('Loading..')
                myDeleteForm.submit()
                myDeleteForm.find('button').prop('disabled', true);   
            }   
        }); 
});
</script>
@endpush
