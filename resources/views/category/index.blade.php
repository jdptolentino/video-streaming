@extends('layouts.app')

@section('content')

    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Categories</h1>
                    </div>
                </div>
            </div>
        </div>
        
        <section class="content">
            <div class="row px-2">
                @if(Session::has('flash_message'))
                    <div class="col-lg-12">
                        <div class="alert alert-success px-2">
                            <span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em>
                        </div>
                    </div>
                @endif
                @if(Session::has('flash_errror_message'))
                    <div class="col-lg-12">
                        <div class="alert alert-danger px-2">
                            <span class="glyphicon glyphicon-down"></span><em> {!! session('flash_errror_message') !!}</em>
                        </div>
                    </div>
                @endif
                
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{ url('/category/create') }}" title="Back"><button class="btn btn-primary btn-sm mt-n2 mb-n1"><i class="fa fa-plus" aria-hidden="true"></i> Add New</button></a>
                            <div class="card-tools">
                                <form method="GET" action="{{ url('/category') }}">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search" value="{{ request('table_search') ? request('table_search') : '' }}">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                                    </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                    <tr>
                                        <th>Name</th><th>Description</th><th width="100">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($category as $item)
                                    <tr>
                                        <!-- <td>{{ $item->id }}</td> -->
                                        <td>{{ $item->name }}</td><td>{{ $item->description }}</td>
                                        <td>
                                            <a href="{{ url('/category/' . $item->id) }}" title="View category"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="{{ url('/category/' . $item->id . '/edit') }}" title="Edit category"><button class="btn btn-primary btn-sm"><i class="fa fa-edit" aria-hidden="true"></i> Edit</button></a>

                                            <form method="POST" action="{{ url('/category' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete category" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @if(count($category) > 0)
                                <div class="pagination-wrapper px-3"> {!! $category->appends(['search' => Request::get('search')])->render() !!} </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
