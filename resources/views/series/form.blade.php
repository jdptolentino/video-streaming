<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="control-label">{{ 'Title' }}</label>
    <input class="form-control" name="title" type="text" id="title" value="{{ isset($series->title) ? $series->title : ''}}" >
    {!! $errors->first('title', '<p class="text-danger help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    <label for="description" class="control-label">{{ 'Description' }}</label>
    <textarea class="form-control" rows="5" name="description" type="textarea" id="description" >{{ isset($series->description) ? $series->description : ''}}</textarea>
    {!! $errors->first('description', '<p class="text-danger help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('slug') ? 'has-error' : ''}}">
    <label for="slug" class="control-label">{{ 'Slug' }}</label>
    <input class="form-control" name="slug" type="text" id="slug" value="{{ isset($series->slug) ? $series->slug : ''}}" >
    {!! $errors->first('slug', '<p class="text-danger help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('photo') ? 'has-error' : ''}}">
    <label for="photo" class="control-label">{{ 'Photo' }}</label>
    <div class="input-group">
        <div class="custom-file">
            <input type='file' class="form-control" id="imgInp" name="photo" accept="image/x-png,image/gif,image/jpeg"/>
            <label class="custom-file-label" for="imgInp">Choose file</label>
        </div>
    </div>
    <br>
    <img id="blah" src="{{ isset($series->photo) ? $series->photo : ''}}" alt="" width="50%"/>
    {!! $errors->first('photo', '<p class="text-danger help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="control-label">{{ 'Status' }}</label>
    <select class="form-control" name="status" id="status">
        @if (isset($series->status))
            <option {{ $series->status == 'ENABLED' ? 'selected': ''}} value="ENABLED">ENABLED</option>
            <option {{ $series->status == 'DISABLED' ? 'selected': ''}} value="DISABLED">DISABLED</option>
        @else
            <option value="ENABLED">ENABLED</option>
            <option value="DISABLED">DISABLED</option>
        @endif
        </select>
    {!! $errors->first('status', '<p class="text-danger help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
