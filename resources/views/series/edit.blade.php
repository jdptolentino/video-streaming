@extends('layouts.app')

@section('content')

    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Edit Series</h1>
                    </div>
                </div>
            </div>
        </div>

        <section class="content">
            <div class="row px-2">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">Edit Series</div>
                        <div class="card-body">
                            <a href="{{ url('/series') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                            <br />
                            <br />

                            @if ($errors->any())
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <li class="ml-2">{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif

                            <form method="POST" action="{{ url('/series/' . $series->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                {{ method_field('PATCH') }}
                                {{ csrf_field() }}

                                @include ('series.form', ['formMode' => 'edit'])

                            </form>

                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">Add/Remove Videos</div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="" class="clear-block">Videos</label>
                                    <ul class="sortable">
                                        @if ($videos)
                                            @foreach($videos as $video)
                                    <li data-id="{{$video->id}}"><span class="badge {{ $video->type === 'PAID' ? 'bg-success' : 'bg-black'}}">{{ $video->type }}</span> {{ $video->title }}</li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <label for="" class="clear-block">Series Videos</label>
                                    <ul class="sortable selected">
                                        @if ($selectedVideos)
                                            @foreach($selectedVideos as $video)
                                                <li data-id="{{$video->id}}"><span class="badge {{ $video->type === 'PAID' ? 'bg-success' : 'bg-black'}}">{{ $video->type }}</span> {{ $video->title }}</li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                            </div>
                            <div class="form-group">
                            <form method="POST" action="{{ url('/series/' . $series->id) }}" accept-charset="UTF-8" class="form-horizontal video_form" enctype="multipart/form-data">
                                {{ method_field('PATCH') }}
                                {{ csrf_field() }}
                                <input type="hidden" value="true" name="videos_edit">
                                <input type="hidden" name="videos">
                                <button type="button" class="btn btn-primary saveVideos">Save</button>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection

@push('custom-scripts')
<script>
        $('.saveVideos').on('click', function(){
            var videos = [];
            $('.sortable.selected li').each(function(i) {
                videos.push($(this).data('id'))
            })

            $('[name="videos"]').val(videos.join(','))

            $('.video_form').submit()
        })
        $(".sortable").on('click', 'li', function (e) {
            if (e.ctrlKey || e.metaKey) {
                $(this).toggleClass("selected");
            } else {
                $(this).addClass("selected").siblings().removeClass('selected');
            }
        }).sortable({
            connectWith: "ul",
            delay: 150, //Needed to prevent accidental drag when trying to select
            revert: 0,
            helper: function (e, item) {
                //Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
                if (!item.hasClass('selected')) {
                    item.addClass('selected').siblings().removeClass('selected');
                }

                //////////////////////////////////////////////////////////////////////
                //HERE'S HOW TO PASS THE SELECTED ITEMS TO THE `stop()` FUNCTION:

                //Clone the selected items into an array
                var elements = item.parent().children('.selected').clone();

                //Add a property to `item` called 'multidrag` that contains the
                //  selected items, then remove the selected items from the source list
                item.data('multidrag', elements).siblings('.selected').remove();

                //Now the selected items exist in memory, attached to the `item`,
                //  so we can access them later when we get to the `stop()` callback

                //Create the helper
                var helper = $('<li/>');
                return helper.append(elements);
            },
            stop: function (e, ui) {
                //Now we access those items that we stored in `item`s data!
                var elements = ui.item.data('multidrag');

                //`elements` now contains the originally selected items from the source list (the dragged items)!!

                //Finally I insert the selected items after the `item`, then remove the `item`, since
                //  item is a duplicate of one of the selected items.
                ui.item.after(elements).remove();
            }

        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function() {
            readURL(this);
        });
    </script>
@endpush
