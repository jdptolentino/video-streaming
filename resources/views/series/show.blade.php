@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">View Series</h1>
                    </div>
                </div>
            </div>
        </div>

        <section class="content">
            <div class="row px-2">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">View Series</div>
                        <div class="card-body">

                            <a href="{{ url('/series') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                            <a href="{{ url('/series/' . $series->id . '/edit') }}" title="Edit series"><button class="btn btn-primary btn-sm"><i class="fa fa-edit" aria-hidden="true"></i> Edit</button></a>

                            <form method="POST" action="{{ url('series' . '/' . $series->id) }}" accept-charset="UTF-8" style="display:inline">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-danger btn-sm" title="Delete series" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                            </form>
                            <br/>
                            <br/>

                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th>ID</th><td>{{ $series->id }}</td>
                                        </tr>
                                        <tr><th> Title </th><td> {{ $series->title }} </td></tr><tr><th> Description </th><td> {{ $series->description }} </td></tr><tr><th> Photo </th><td> <img id="blah" src="{{ $series->photo }}" alt="" width="50%"/> </td></tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">Videos</div>
                            <div class="card-body p-0">
                                <table class="table">
                                    <tbody>
                                        @if ($selectedVideos)
                                            @foreach($selectedVideos as $video)
                                            <tr>
                                                <td> <a href="{{ url('video/' . $video->id) }}">{{ $video->title }}</a></td>
                                            </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
