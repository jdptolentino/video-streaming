@extends('layouts.app')

@section('content')

	<div class="content-wrapper">
		<div class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 class="m-0 text-dark">Requests</h1>
					</div>
				</div>
			</div>
		</div>
		@if (count($subscription) > 0)
			@foreach ($subscription as $item)
				<div class="modal fade" id="modal{{ $item->id }}" role="dialog" aria-labelledby="exampleModalLabel"
					aria-hidden="true">
					<div class="modal-dialog">
						<form method="POST" action="{{ url('/requests' . '/' . $item->id . '/grant') }}" accept-charset="UTF-8">
							{{ csrf_field() }}
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">Grant access</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									Are you sure to grant access?
									<br>
									<br>
									<div><b>Subscription: {{ $item->plan->title }}</b></div>
									<div><b>User amount paid: {{ number_format($item->plan->amount, 2) }}</b></div>
									<br>
									<label for="">Confirm subscription</label><br>
									<select name="plan" id="" class="form-control">
										@foreach ($subscriptions as $v)
											<option {{ $item->plan->id === $v->id ? 'selected' : '' }} value="{{ $v->id }}">
												{{ $v->title }} - {{ number_format($v->amount, 2) }}</option>
										@endforeach
									</select>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									<button type="button" class="btn btn-primary submit">Accept</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			@endforeach
		@endif
		<section class="content">
			<div class="row px-2">
				@if (Session::has('flash_message'))
					<div class="col-lg-12">
						<div class="alert alert-success px-2">
							<span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em>
						</div>
					</div>
				@endif

				@if (Session::has('flash_message_info'))
					<div class="col-lg-12">
						<div class="alert alert-info px-2">
							<span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message_info') !!}</em>
						</div>
					</div>
				@endif

				@if (Session::has('flash_message_error'))
					<div class="col-lg-12">
						<div class="alert alert-danger px-2">
							<span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message_error') !!}</em>
						</div>
					</div>
				@endif

				<div class="col-lg-12">
					<div class="card">
						<div class="card-header">
							<div class="card-tools">
								<form method="GET" action="{{ url('/requests') }}">
									<div class="input-group input-group-sm" style="width: 150px;">
										<input type="text" name="table_search" class="form-control float-right" placeholder="Search">
										<div class="input-group-append">
											<button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
										</div>
									</div>
								</form>
							</div>
						</div>

						<div class="card-body table-responsive p-0">
							<table class="table table-hover text-nowrap">
								<thead>
									<tr>
										<th>Username</th>
										<th>Email</th>
										<th>Subscription</th>
										<th>Amount</th>
										<th>Status</th>
										<th width="100">Actions</th>
									</tr>
								</thead>
								<tbody>
									@if (count($subscription) == 0)
										<tr>
											<td colspan="100%">
												<div class="alert alert-danger">No data found</div>
											</td>
										</tr>
									@else
										@foreach ($subscription as $item)
											<tr>
												<!-- <td>{{ $item->id }}</td> -->
												<td>{{ $item->user['user_name'] }}</td>
												<td>{{ $item->user['email'] }}</td>
												<td>{{ $item->plan->title }}</td>
												<td>{{ number_format($item->plan->amount, 2) }}</td>
												<td>
													<span class="badge bg-warning">{{ $item->status }}</span>
												</td>
												<td>
													<!-- <form method="POST" action="{{ url('/requests' . '/' . $item->id . '/grant') }}" accept-charset="UTF-8" style="display:inline">
																																																					{{ csrf_field() }}
																																																					<button type="submit" class="btn btn-danger btn-sm" title="Grant access" onclick="return confirm(&quot;Are you sure to grant access?&quot;)">Grant access</button>
																																																	</form> -->
													<button type="button"class="btn btn-warning btn-sm text-white" title="Grant access" data-toggle="modal"
														data-target="#modal{{ $item->id }}">
														<i class="fa fa-check" aria-hidden="true"></i> Grant access
													</button>



													<!-- delete start-->
													<button type="button"class="btn btn-danger btn-sm" title="Delete request" data-toggle="modal"
														data-target="#deleteModal{{ $item->id }}">
														<i class="fa fa-trash" aria-hidden="true"></i> Delete
													</button>

													<div class="modal fade" id="deleteModal{{ $item->id }}" tabindex="-1" role="dialog"
														aria-labelledby="exampleModalLabel" aria-hidden="true">
														<div class="modal-dialog">
															<form method="POST" action="{{ url('/requests' . '/destroy/' . $item->id) }}" accept-charset="UTF-8">
																{{ csrf_field() }}
																<div class="modal-content">
																	<div class="modal-header">
																		<h5 class="modal-title" id="exampleModalLabel">Delete request</h5>
																		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																			<span aria-hidden="true">&times;</span>
																		</button>
																	</div>
																	<div class="modal-body">
																		Are you sure to delete this request?
																		<br>
																		<br>
																		<div><b>Subscription: {{ $item->plan->title }}</b></div>
																		<div><b>User amount paid: {{ number_format($item->plan->amount, 2) }}</b></div>
																	</div>
																	<div class="modal-footer">
																		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
																		<button type="button" class="btn btn-primary submitDelete">Delete</button>
																	</div>
																</div>
															</form>
														</div>
													</div>
													<!-- delete end -->
												</td>
											</tr>
										@endforeach
									@endif
								</tbody>
							</table>
							@if (count($subscription) > 0)
								<div class="pagination-wrapper px-3"> {!! $subscription->appends(['search' => Request::get('search')])->render() !!} </div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	@push('custom-scripts')
		<script>
		 $(document).ready(function() {
		  $('button.submit').on('click', function() {
		   var myForm = $(this).closest("form");
		   if (myForm) {
		    $(this).text('Loading..')
		    myForm.submit()
		    myForm.find('button, select').prop('disabled', true);
		   }
		  });

		  // delete
		  $('button.submitDelete').on('click', function() {
		   var myDeleteForm = $(this).closest("form");
		   if (myDeleteForm) {
		    $(this).text('Loading..')
		    myDeleteForm.submit()
		    myDeleteForm.find('button').prop('disabled', true);
		   }
		  });
		 });
		</script>
	@endpush
@endsection
