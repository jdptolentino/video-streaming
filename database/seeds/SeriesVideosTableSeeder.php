<?php

use Illuminate\Database\Seeder;

class SeriesVideosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('series_videos')->insert([
            [
                'series_id' => 1,
                'video_id' => 1,
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ],
            [
                'series_id' => 1,
                'video_id' => 2,
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ]
        ]);
    }
}
