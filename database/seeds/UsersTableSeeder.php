<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $token = Str::random(60);

        DB::table('users')->insert([
            'first_name' => 'John',
            'middle_name' => 'Papa',
            'last_name' => 'Doe',
            'user_name' => 'ianromie',
            'email' => 'ianromie@gmail.com',
            'password' => Hash::make('password'),
            'status' => 'ACTIVE',
            'activation_token' => Str::random(60),
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d')
        ]);
    }
}
