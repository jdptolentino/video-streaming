<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Video;

class SeriesVideos extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'series_videos';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['series_id', 'video_id'];

    /**
     * Relationships
     */

    public function video() {
        return $this->hasOne(Video::class, 'id', 'video_id')->with('category');
    }


}
