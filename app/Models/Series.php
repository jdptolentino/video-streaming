<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Series extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'series';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'slug', 'photo', 'status'];

    protected $appends = [
        'videoCount'
    ];

    /**
     * Mutator
     * Photo URL
     * @param $value
     * @return string
     */
    public function getPhotoAttribute($value) {
        if (!strlen($value)) {
            return env('API_URL') . '/images/series.png';
        }

        return env('API_URL') . '/images/series/' . $value;
    }

     /**
     * Mutator
     * Photo URL
     */
    public function getVideoCountAttribute() {
        return $this->video()->count();
    }

    // Relationships

    public function video() {
        return $this->hasMany(\App\Models\SeriesVideos::class, 'series_id', 'id');
    }

}
