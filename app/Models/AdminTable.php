<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminTable extends Authenticatable
{
    use HasApiTokens, Notifiable, SoftDeletes;

    protected $table = 'admins';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'middle_name', 'last_name', 'email', 'password', 'status', 'type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function scopeSearchNameEmail($query, $keyword){
        $query->where('first_name', 'like', '%' . $keyword . '%')
        ->orWhere('middle_name', 'like', '%' . $keyword . '%')
        ->orWhere('last_name', 'like', '%' . $keyword . '%')
        ->orWhere('email', 'like', '%' . $keyword . '%');
    }
}
