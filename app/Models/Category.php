<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    
    protected $table = 'categories';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];

    protected $appends = [
        'slug'
    ];

    /**
     * Mutators
     */
    public function getSlugAttribute() {
        $name = strtolower($this->name);
        
        return str_replace(' ', '-', $name);
    }
    
}
