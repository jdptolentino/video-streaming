<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'transactions';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'subscription_plan_id', 'status', 'amount', 'external_response'];


    public function user() {
        return $this->hasOne(\App\User::class, 'id', 'user_id');
    }

    public function plan() {
        return $this->hasOne(\App\Models\SubscriptionPlan::class, 'id', 'subscription_plan_id');
    }
}
