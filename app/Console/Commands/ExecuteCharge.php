<?php

namespace App\Console\Commands;

use App\User;
use App\Models\Subscription;
use App\Jobs\ChargeUserJob;
use Illuminate\Console\Command;

class ExecuteCharge extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'charge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Charge user for subscription';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Execute subscriptions check.<br/>";
        echo "---------------------------------<br/>";
        $subs = Subscription::where('status', 'ACTIVE')->get();
        foreach ($subs as $sub) {
            if($sub->user && $sub->user->status == 'ACTIVE'){
                $this->runExpiry($sub->user);
            }
        }
        echo "---------------------------------<br/>";
        echo "Active user with lapse subscription set to LAPSED status. <br/> You may close this tab.";
    }

    /**
     * Process CANCELLED Subscription
     */
    public function runSuspension($user) {
        $txn = $user->latestTransaction->first();

        if (!$this->hasDaysLeft($txn)) {
            // Suspend User
            $user->status = "SUSPEND";
            $user->save();

            // Set Subscription to LAPSED
            $user->plan->status = "LAPSED";
            $user->plan->save();
        }
    }

    /**
     * Process set to lapsed Subscription
     */
    public function runExpiry($user) {
        $txn = $user->latestTransaction->first();

        if (!$this->hasDaysLeft($txn)) {
            echo "User: (" . $user->id . ")" . $user->full_name . " - subscription expired. <br>"; 

            // Set Subscription to LAPSED
            $user->activePlans->status = "LAPSED";
            $user->activePlans->save();
        }


        //For test
        //if (!$this->toExpire($txn)) {
        //    // Set Subscription to LAPSED
        //    $user->plan->status = "LAPSED";
        //    $user->plan->save();
        //}
    }


    /**
     * Charge User
     */
    public function runCharge($user) {
        $txn = $user->latestTransaction->first();

        if (!$this->hasDaysLeft($txn)) {
            if ($txn->status === 'PAID') {
                // dispatch(new ChargeUserJob($user));
            }
        }
    }

    /**
     * Check if user has days left in subscription
     */
    public function hasDaysLeft($txn) {
        $now = time();
        $txnDate = strtotime($txn->created_at);
        $endDate = strtotime("+" . $txn->plan->frequency . " " . $this->frequencyType($txn->plan->frequency_type), $txnDate);

        $datediff = $endDate - $now;

        return date("Y/m/d", $endDate) > date("Y/m/d", $now ) ? true : false;
    }

    /**
     * Check if user has days left in subscription - One day for test
     */
    public function toExpire($txn) {
        $now = time();
        $txnDate = strtotime($txn->created_at);
        $endDate = strtotime("+1 day", $txnDate);

        $datediff = $endDate - $now;

        return date("Y/m/d", $endDate) > date("Y/m/d", $now ) ? true : false;
    }

    /**
     * Get Plan frequency
     */
    private function frequencyType($type) {

        switch ($type) {
            case $type == 'DAY':
                return 'day';
            case $type == 'MONTH':
                return 'month';
            case $type == 'YEAR':
                return 'year';
            default:
                return 'month';
        }

    }


}
