<?php

namespace App\Console\Commands;

use App\User;
use App\Models\Subscription;
use App\Jobs\ChargeUserJob;
use Illuminate\Console\Command;

class RevertCharge extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'revertcharge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Revert Charge user for subscription';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Execute subscriptions check.<br/>";
        echo "---------------------------------<br/>";

        $subs = Subscription::where('status', 'LAPSED')->get();
        foreach ($subs as $sub) {
            if($sub->user->status == 'ACTIVE'){
                $this->runRevert($sub->user);
            }
        }

        echo "---------------------------------<br/>";
        echo "Active user with lapsed subscription set to ACTIVE status. <br/> You may close this tab.";
    }

    /**
     * Process set to lapsed Subscription
     */
    public function runRevert($user) {
        $txn = $user->latestTransaction->first();

        if ($this->hasDaysLeft($txn)) {
            echo "User: (" . $user->id . ")" . $user->full_name . " - subscription set to active. <br>"; 

            // Set Subscription to LAPSED
            $user->lapsedPlans->status = "ACTIVE";
            $user->lapsedPlans->save();
        }

    }


    /**
     * Check if user has days left in subscription
     */
    public function hasDaysLeft($txn) {
        $now = time();
        $txnDate = strtotime($txn->created_at);
        $endDate = strtotime("+" . $txn->plan->frequency . " " . $this->frequencyType($txn->plan->frequency_type), $txnDate);

        $datediff = $endDate - $now;

        return date("Y/m/d", $endDate) > date("Y/m/d", $now ) ? true : false;
    }

    /**
     * Get Plan frequency
     */
    private function frequencyType($type) {

        switch ($type) {
            case $type == 'DAY':
                return 'day';
            case $type == 'MONTH':
                return 'month';
            case $type == 'YEAR':
                return 'year';
            default:
                return 'month';
        }

    }


}
