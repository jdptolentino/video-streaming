<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    use SoftDeletes;
    
    public $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'middle_name', 'last_name', 'user_name',  'email', 'password', 'activation_token', 'status', 'type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];


    public static function boot()
    {
        parent::boot();    
        // cause a delete of a product to cascade to children so they are also deleted
        static::deleted(function($user)
        {
            $user->transactions()->delete();
            $user->userSubscription()->delete();
        });
    }

    /**
     * Mutator
     * Full name
     */
    public function getFullNameAttribute() {
            return "{$this->first_name} {$this->last_name}";
    }

    /**
     * Relationships
     */

    public function transactions() {
        return $this->hasMany(\App\Models\Transaction::class, 'user_id');
    }

    public function latestTransaction() {
        return $this->hasMany(\App\Models\Transaction::class, 'user_id')->latest();
    }

    public function plan() {
        return $this->hasOne(\App\Models\Subscription::class, 'user_id')->whereIn('status', ['ACTIVE', 'CANCELLED', 'LAPSED'])->latest();
    }

    public function activePlans() {
        return $this->hasOne(\App\Models\Subscription::class, 'user_id')->where('status', 'ACTIVE')->latest();
    }

    public function lapsedPlans() {
        return $this->hasOne(\App\Models\Subscription::class, 'user_id')->where('status', 'LAPSED')->latest();
    }

    public function subscription() {
        return $this->hasOne(\App\Models\Subscription::class, 'user_id')->with('plan')->whereIn('status', ['ACTIVE', 'LAPSED'])->latest();
    }

    public function userSubscription() {
        return $this->hasOne(\App\Models\Subscription::class, 'user_id');
    }

}
