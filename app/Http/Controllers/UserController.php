<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\Models\SubscriptionPlan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('table_search');
        $perPage = 25;

        if (!empty($keyword)) {
            $user = User::where('first_name', 'like', '%' . $keyword . '%')
            ->orWhere('user_name', 'like', '%' . $keyword . '%')
            ->orWhere('middle_name', 'like', '%' . $keyword . '%')
            ->orWhere('last_name', 'like', '%' . $keyword . '%')
            ->orWhere('email', 'like', '%' . $keyword . '%')
            ->latest()->paginate($perPage);
        } else {
            $user = User::latest()->paginate($perPage);
        }

        return view('user.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();
        
        $validator = Validator::make($requestData, [
            // 'first_name' => 'required|string|max:255',
            // 'last_name' => 'required|string|max:255',
            'user_name' => 'nullable|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect('user/create')
            ->withInput()
            ->withErrors($validator);
        }

        $requestData['password'] = Hash::make($requestData['password']);
        $requestData['activation_token'] = Str::random(60);

        User::create($requestData);

        return redirect('user')->with('flash_message', 'User added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        $subscriptions = SubscriptionPlan::where('status', 'ENABLED')->get();

        return view('user.show', compact('user', 'subscriptions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        return view('user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();

        $validator = Validator::make($requestData, [
            // 'first_name' => 'required|string|max:255',
            // 'last_name' => 'required|string|max:255',
            'user_name' => 'nullable|string|max:255|unique:users,user_name,'.$id,
            'email' => 'required|string|email|max:255|unique:users,email,'.$id,
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $user = User::findOrFail($id);
        $user->update($requestData);

        return redirect('user')->with('flash_message', 'User details updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        User::destroy($id);
        
        return redirect('user')->with('flash_message', 'User deleted!');
    }

    public function removeUnverified()
    {
        $data = User::where('status','EMAIL_VERIFICATION')
        ->where('created_at', '<', \Carbon\Carbon::now()->subWeek())
        ->orderBy('created_at','DESC')
        ->forceDelete();
    }
}
