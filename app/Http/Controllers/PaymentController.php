<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \PayPal\Rest\ApiContext;
use \PayPal\Auth\OAuthTokenCredential;
use \PayPal\Api\Agreement;
use \PayPal\Api\Payer;
use \PayPal\Api\Plan;
use \PayPal\Api\PaymentDefinition;
use \PayPal\Api\PayerInfo;
use \PayPal\Api\Item;
use \PayPal\Api\ItemList;
use \PayPal\Api\Amount;
use \PayPal\Api\Transaction;
use \PayPal\Api\RedirectUrls;
use \PayPal\Api\Payment;
use \PayPal\Api\PaymentExecution;
use Illuminate\Support\Facades\Input;
use Redirect;
use URL;

use App\Models\Subscription;
use App\User;
use App\Notifications\AccessGranted;
use App\Models\SubscriptionPlan;
use App\Jobs\AccessGrantedEmail;

class PaymentController extends Controller
{
    public function __construct()
    {
         /** PayPal api context **/
         $paypal_conf = \Config::get('paypal');
         $this->_api_context = new ApiContext(new OAuthTokenCredential(
             $paypal_conf['client_id'],
             $paypal_conf['secret'])
         );
         $this->_api_context->setConfig($paypal_conf['settings']);
    }

    public function payWithpaypal(Request $request)
    {

        if (!isset($request->user_id) || !isset($request->plan_id)) {
            return response()->json([
                "message" => 'Some error occur, sorry for inconvenient'
            ], 500);
        }

        $plan = SubscriptionPlan::find($request->plan_id);

        $amountToBePaid = $plan->amount;
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
  
        $item_1 = new Item();
        $item_1->setName('HeyPogi - ' . $plan->title) /** item name **/
                ->setCurrency('PHP')
                ->setQuantity(1)
                ->setPrice($amountToBePaid); /** unit price **/
  
        $item_list = new ItemList();
        $item_list->setItems(array($item_1));
  
        $amount = new Amount();
        $amount->setCurrency('PHP')
                ->setTotal($amountToBePaid);
        $redirect_urls = new RedirectUrls();
        /** Specify return URL **/
        $redirect_urls->setReturnUrl(URL::route('status') . '?user_id=' . $request->user_id . '&plan_id=' . $request->plan_id)
                    ->setCancelUrl(URL::route('status'));
      
        $transaction = new Transaction();
        $transaction->setAmount($amount)
                ->setItemList($item_list)
                ->setDescription($plan->description);   
   
        $payment = new Payment();
        $payment->setIntent('Sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirect_urls)
                ->setTransactions(array($transaction));
        try {
           $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
           if (\Config::get('app.debug')) {
              return response()->json([
                "message" => 'Connection timeout'
            ], 408);
           } else {
                return response()->json([
                    "message" => 'Some error occur, sorry for inconvenient'
                ], 500);
           }
        }
        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
      
        if (isset($redirect_url)) {
            return response()->json([
                "link" => $redirect_url,
                "id" => $payment->getId(),
            ], 200);
        }

        return response()->json([
            "message" => 'Unknown error occurred'
        ], 500);
    }
    
    public function getPaymentStatus(Request $request)
    {

      /** Get the payment ID before session clear **/
      $payment_id = $request->paymentId;
      /** clear the session payment ID **/

      if (empty($request->PayerID) || empty($request->token)) {
         return Redirect::away(env('WEB_APP_URL') . '?status=error');
      }

      $payment = Payment::get($payment_id, $this->_api_context);
      $execution = new PaymentExecution();
      $execution->setPayerId($request->PayerID);

      /**Execute the payment **/
      $result = $payment->execute($execution, $this->_api_context);
      
        if ($result->getState() == 'approved') {
            $user = User::find($request->user_id);
            Subscription::applySubs($user, $request->plan_id);
            $subscription = Subscription::with(['plan', 'user'])->where('user_id', $request->user_id)->where('plan_id', $request->plan_id)->latest('created_at')->first();
            if ($subscription->status === "REQUESTED") {
                $plan = SubscriptionPlan::find($request->plan_id);
                if ($subscription) {
                    $subscription->status = 'ACTIVE';
                    $subscription->plan_id = $request->plan_id;
                    $txn = new \App\Models\Transaction;
                    $txn->user_id = $subscription->user_id;
                    $txn->subscription_plan_id = $request->plan_id;
                    $txn->status = 'PAID';
                    $txn->amount = $plan->amount;
                    $txn->external_response = "{'paid': true, 'platform': 'manual'}";
                    $subscription->save();
                    $txn->save();
        
                    $user = User::find($subscription->user_id);
                    $user->notify(
                        new AccessGranted($user)
                    );
                    return Redirect::away(env('WEB_APP_URL') . '?status=success');
                }
            }
            return Redirect::away(env('WEB_APP_URL') . '?status=error');
        }
        return Redirect::away(env('WEB_APP_URL') . '?status=error');
    }
}
