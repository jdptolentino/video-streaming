<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\SubscriptionPlan;
use Illuminate\Http\Request;

class SubscriptionPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $plan = SubscriptionPlan::latest()->paginate($perPage);
        } else {
            $plan = SubscriptionPlan::latest()->paginate($perPage);
        }

        return view('plan.index', compact('plan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('plan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        $validator = Validator::make($requestData, [
            'title' => 'required|string|max:255',
            'amount' => 'required|',
            'frequency' => 'required|numeric',
            'status' => 'required|string',
        ]);

        if ($validator->fails()) {
            return redirect('plan/create')
            ->withInput()
            ->withErrors($validator);
        }
        
        SubscriptionPlan::create($requestData);

        return redirect('plan')->with('flash_message', 'Subscription Plan added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $plan = SubscriptionPlan::findOrFail($id);

        return view('plan.show', compact('plan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $plan = SubscriptionPlan::findOrFail($id);

        return view('plan.edit', compact('plan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $validator = Validator::make($requestData, [
            'title' => 'required|string|max:255',
            'amount' => 'required|',
            'frequency' => 'required|numeric',
            'status' => 'required|string',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $plan = SubscriptionPlan::findOrFail($id);
        $plan->update($requestData);

        return redirect('plan')->with('flash_message', 'Subscription Plan details updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        // SubscriptionPlan::destroy($id);

        // return redirect('plan')->with('flash_message', 'Subscription Plan deleted!');
    }
}
