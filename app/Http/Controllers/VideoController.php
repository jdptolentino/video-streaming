<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Video;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('table_search');
        $perPage = 25;

        if (!empty($keyword)) {
            $video = Video::where('title', 'like', '%' . $keyword .  '%')
                ->orWhere('description', 'like', '%' . $keyword .  '%')
                ->orWhere('id', $keyword)
                ->latest()->paginate($perPage);
        } else {
            $video = Video::latest()->paginate($perPage);
        }

        return view('video.index', compact('video'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $categories = \App\Models\Category::get();
        return view('video.create')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        $validator = Validator::make($requestData, [
            'title' => 'required',
            'link' => 'required',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        if ($request->file('photo')) {
            $md5Name = md5_file($request->file('photo')->getRealPath());
            $guessExtension = $request->file('photo')->guessExtension();
    
            $request->file('photo')->storeAs("/", $md5Name.'.'.$guessExtension  , 'public');
            $requestData['photo'] =  $md5Name.'.'.$guessExtension;
        }
        
        $requestData['isEmbed'] = isset($requestData['isEmbed']) ? 1 : 0;
        
        Video::create($requestData);

        return redirect('video')->with('flash_message', 'Video added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $video = Video::findOrFail($id);

        return view('video.show', compact('video'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $video = Video::findOrFail($id);
        $categories = \App\Models\Category::get();
        return view('video.edit', compact('video', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $validator = Validator::make($requestData, [
            'title' => 'required',
            'link' => 'required',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        
        $video = Video::findOrFail($id);

        $requestData['isEmbed'] = isset($requestData['isEmbed']) ? 1 : 0;

        if ($request->file('photo')) {
            $md5Name = md5_file($request->file('photo')->getRealPath());
            $guessExtension = $request->file('photo')->guessExtension();
    
            $request->file('photo')->storeAs("/", $md5Name.'.'.$guessExtension  , 'public');
            $requestData['photo'] =  $md5Name.'.'.$guessExtension;
        }
        $video->update($requestData);

        return redirect('video')->with('flash_message', 'Video details updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Video::destroy($id);

        return redirect('video')->with('flash_message', 'Video deleted!');
    }
}
