<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Subscription;
use App\Models\Transaction;
use App\User;
use App\Notifications\AccessGranted;
use App\Models\SubscriptionPlan;
use App\Jobs\AccessGrantedEmail;

class RequestsController extends Controller
{
    public function index(Request $request) {
        $keyword = $request->get('table_search');
        $perPage = 25;
        if (!empty($keyword)) {
            $subscription = Subscription::whereHas('user',function($query) use ($keyword) {
                $query->where('email', 'like', '%' . $keyword . '%')
                    ->orWhere('user_name', 'like', '%' . $keyword . '%');
                })
                ->with(['user' => function($query) use ($keyword){
                    $query->where('email', 'like', '%' . $keyword . '%')
                        ->orWhere('user_name', 'like', '%' . $keyword . '%');
                }])
                ->with(['plan', 'user'])
                ->where('status', 'REQUESTED')
                ->latest()->paginate($perPage);
        } else {
            $subscription = Subscription::with(['plan', 'user'])->where('status', 'REQUESTED')->latest()->paginate($perPage);
        }

        $subscriptions = SubscriptionPlan::all();
        return view('requests.index', compact('subscription', 'subscriptions'));
    }

    public function grantAccess($id) {
        $subscription = Subscription::with(['plan', 'user'])->find($id);
        if ($subscription->status === "REQUESTED") {
            $plan = SubscriptionPlan::find(request()->plan);
            if ($subscription) {
                $subscription->status = 'ACTIVE';
                $subscription->plan_id = request()->plan;
                $txn = new \App\Models\Transaction;
                $txn->user_id = $subscription->user_id;
                $txn->subscription_plan_id = request()->plan;
                $txn->status = 'PAID';
                $txn->amount = $plan->amount;
                $txn->external_response = "{'paid': true, 'platform': 'manual'}";
                $subscription->save();
                $txn->save();
    
                $user = User::find($subscription->user_id);
                $user->notify(
                    new AccessGranted($user)
                );
                return redirect('requests')->with('flash_message', 'Grant access successfully');
            }
        }else{
            return redirect('requests')->with('flash_message_info', 'The subscription request is already processed');
        }

        return redirect('requests')->with('flash_message_error', 'Error Occured!');
    }

    public function backOfficeAddSub(Request $request, $id) {

        $user = User::find($id);
        Subscription::applySubs($user, $request->plan);

        return redirect('requests')->with('flash_message_info', 'Subscription has been placed.');
    }

    public function destroy($id){
        $data = Subscription::findOrFail($id);
        if($data->update(['status'=>'CANCELLED'])){
            return redirect('requests')->with('flash_message', 'Request deleted!');
        }else{
            return redirect('requests')->with('flash_message_error', 'Unable to process request!');
        }
    }
}
