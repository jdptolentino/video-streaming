<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Transaction;
use App\Models\Subscription;
use App\Models\Video;
use App\Models\SubscriptionPlan;

use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countTxn = Transaction::where('status', 'PAID')->count();
        $countSales = Transaction::where('status', 'PAID')->sum('amount');
        $lastMonthSales = Transaction::whereMonth(
            'created_at', '=', Carbon::now()->subMonth()->month
        )->whereYear('created_at', Carbon::now()->subMonth()->year)->where('status', 'PAID')->sum('amount');
        $currentMonthSales = Transaction::whereMonth('created_at', Carbon::now()->month)->whereYear('created_at', Carbon::now()->year)->where('status', 'PAID')->sum('amount');
        $countUsers = User::count();
        $countVideos = Video::count();
        $transactions = Transaction::limit(6)->orderBy('created_at','DESC')->get();
        $requests = Subscription::where('status', 'REQUESTED')->limit(6)->orderBy('created_at','DESC')->get();
        $users = User::limit(5)->orderBy('created_at','DESC')->get();
        $subscriptions = SubscriptionPlan::all();
        return view('home', compact('countTxn', 'countUsers', 
        'countVideos', 'countSales', 
        'transactions', 'users', 'requests', 
        'subscriptions', 'lastMonthSales', 'currentMonthSales'));
    }
}
