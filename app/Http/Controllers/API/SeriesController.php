<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Series;
use App\Models\Video;
use App\Models\SeriesVideos;

class SeriesController extends Controller
{
    /**
     * Get all series
     */
    public function all(Request $request) {
        $perPage = $request->has('perPage') ? $request->perPage : 10;
        $page = $request->has('page') ? $request->page : 1;
        $skip = ($page - 1) * $perPage;
        $search = $request->search;

        $series = Series::where('status', 'ENABLED')->where(function($query) use ($search) {
            if ($search) {
                $query->where('title', 'like', '%' . $search . '%');
            }
        });
        $seriesItems = $series->skip($skip)->take($perPage)->orderBy('order', 'ASC')->get();

        return response()->json([
            "total" => $series->count(),
            "perPage" => (int)$perPage,
            "page" => (int)$page,
            "data" => $seriesItems,
        ], 200);
    }

    /**
     * Get search home
     */
    public function search(Request $request) {
        $perPage = $request->has('perPage') ? $request->perPage : 10;
        $page = $request->has('page') ? $request->page : 1;
        $skip = ($page - 1) * $perPage;
        $search = $request->search;

        // Series
        $series = Series::where('status', 'ENABLED')->where(function($query) use ($search) {
            if ($search) {
                $query->where('title', 'like', '%' . $search . '%');
            }
        });
        $seriesItems = $series->orderBy('order', 'ASC')->get();

        // Videos
        
        $videos = Video::with('category')->where('title', 'like', '%' . $search .  '%')
        // ->orWhere('description', 'like', '%' . $search .  '%')
        ->latest()->get();

        $search = new \stdClass();
        $search->series = $seriesItems;
        $search->videos = $videos;

        return response()->json([
            "data" => $search,
        ], 200);
    }

    /**
     * Get series by slug
     */
    public function getSeriesBySlug(Request $request, $slug) {
        // dd($slug);
        $perPage = $request->has('perPage') ? $request->perPage : 10;
        $page = $request->has('page') ? $request->page : 1;
        $skip = ($page - 1 ) * $perPage;

        $series = Series::where('slug', $slug)->first();
        $search = $request->search;

        if ($series) {

            // $videos = SeriesVideos::with(['video' => function($query) use ($search) {
            //     $query->where('title', 'like', '%' . $search .  '%');
                
            // }])->where('series_id', $series->id)->orderBy('created_at', 'DESC');


            $seriesAndVideo = SeriesVideos::where('series_id', $series->id)->orderBy('created_at', 'DESC');
            $videoIds = $seriesAndVideo->get()->pluck('video_id')->toArray();

            $videos = Video::with('category')->whereIn('id', $videoIds)->where('title', 'like', '%' . $search .  '%');

            $videosArray = array_values(array_filter($videos->get()->toArray()));
            $tempArray = [];

            foreach($videoIds as $i ) {
                foreach($videosArray as $v ) {
                    if($v['id'] === $i) {
                        $tempArray[] = $v;
                    }
                }
            }
            
            $tempArray = array_slice( $tempArray, $skip, $perPage );
            
            // dd(array_values(array_filter($videos->skip($skip)->take($perPage)->get()->toArray())));
            return response()->json([
                "data" => [
                    'series' => $series,
                    "total" => $videos->count(),
                    'videos' => $tempArray,
                    // 'videos' => array_values(array_filter($videos->skip($skip)->take($perPage)->get()->pluck('video')->toArray())),
                    "perPage" => (int)$perPage,
                    "page" => (int)$page,
                ]
            ], 200);
        }


        return response()->json([
            "message" => 'Series not found.'
        ], 400);
    }
}
