<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    private $user;

    public function __construct(Request $request) {
        if ($request->header('x-user-id')) {
            $this->user = $request->header('x-user-id');
        }
    }
    public function getAll(Request $request) {

        if ($this->user) {
            $perPage = $request->has('perPage') ? $request->perPage :  10;
            $page = request('page') ? request('page') : 0;
            $skip = ($page - 1) * $perPage;
            $txn = Transaction::with(['plan', 'user'])->where('status', 'PAID')->where('user_id', $this->user);
            $txnItems = $txn->skip($skip)->take($perPage)->get();

            return response()->json([
                "total" => $txn->count(),
                "perPage" => (int)$perPage,
                "page" => (int)$page,
                "data" => $txnItems,
                "error" => false
            ], 200);
        }

    }
}
