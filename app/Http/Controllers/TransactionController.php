<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('table_search');
        $perPage = 25;

        if (!empty($keyword)) {
            $transactions = Transaction::whereHas('user',function($query) use ($keyword) {
                $query->where('first_name', 'like', '%' . $keyword .  '%')
                    ->orWhere('last_name', 'like', '%' . $keyword .  '%');
                })
                ->with(['user' => function($query) use ($keyword){
                    $query->where('first_name', 'like', '%' . $keyword .  '%')
                        ->orWhere('last_name', 'like', '%' . $keyword .  '%');
                }])
                ->orWhere('id', $keyword)
                ->with(['user','plan'])
                ->latest()->paginate($perPage);
        } else {
            $transactions = Transaction::latest()->paginate($perPage);
        }
        
        return view('transactions.index', compact('transactions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('transactions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Transaction::create($requestData);

        return redirect('transactions')->with('flash_message', 'Transaction added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $transaction = Transaction::findOrFail($id);

        return view('transactions.show', compact('transaction'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $transaction = Transaction::findOrFail($id);

        return view('transactions.edit', compact('transaction'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $transaction = Transaction::findOrFail($id);
        $transaction->update($requestData);

        return redirect('transactions')->with('flash_message', 'Transaction updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Transaction::destroy($id);

        return redirect('transactions')->with('flash_message', 'Transaction deleted!');
    }

    
    /**
     * View transaction invoice
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function invoice($id)
    {
        $transaction = Transaction::findOrFail($id);

        return view('transactions.invoice', compact('transaction'));
    }
}
