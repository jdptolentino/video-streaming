<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function(){

    // Admin profile
    Route::get('profile', 'AdminController@profile');
    Route::get('profile/edit', 'AdminController@profileEdit');
    Route::patch('profile/edit', 'AdminController@profileUpdate');
    Route::patch('profile/edit-password', 'AdminController@profilePasswordUpdate');

    Route::group(['middleware' => 'adminOnly'], function(){
        Route::resource('category', 'CategoryController');
        Route::resource('user', 'UserController');
        Route::resource('video', 'VideoController');
        Route::resource('plan', 'SubscriptionPlanController');
        Route::resource('transactions', 'TransactionController');
        Route::get('series/reorder', 'SeriesController@reorder');
        Route::patch('series/reorder', 'SeriesController@reorderSave');
        Route::resource('series', 'SeriesController');
        Route::resource('admin', 'AdminController');
        Route::get('transactions/invoice/{id}', 'TransactionController@invoice');

        // Requests
        Route::get('requests', 'RequestsController@index');
        Route::post('requests/{id}/grant', 'RequestsController@grantAccess');
        Route::post('requests/destroy/{id}', 'RequestsController@destroy');
        Route::post('requests/back/{id}', 'RequestsController@backOfficeAddSub');

        // reports
        Route::get('reports', 'ReportController@index');
        Route::post('reports', 'ReportController@index');

        // export
        Route::post('export/requests', 'ReportController@exportRequests');
        Route::post('export/users', 'ReportController@exportUsers');

        // Run execution of charge
        Route::get('run-charge',function() {
            Artisan::call("charge");
        });

        // Revert execution of charge
        Route::get('revert-charge',function() {
            Artisan::call("revertcharge");
        });
    });
    
});

Route::get('/thumbnail-reponse/{id}', 'MiscController@thumbnail');
Route::get('remove-unverified-user', 'UserController@removeUnverified');

